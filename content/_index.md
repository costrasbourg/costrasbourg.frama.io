---
date: "2018-08-26T18:27:58+01:00"
title: "Accueil"
---

## Que l'on soit un citoyen·ne ou une organisation :

* **nous avons tous des ressources à mettre en commun** : compétence, matériel, connaissance, ...
* **nos besoins respectifs sont similaires** : trouver un lieu où se réunir, communiquer, accueillir de nouveaux bénévoles, ...
* **nous bénéficions tous de l'entraide**. Une heure à coopérer, c'est parfois plusieurs heures de gagnées dans nos actions futures. `#InvestissonsDansLaCoopération`
* mais... notre engagement nous empêche de consacrer du temps à la coopération. `#NezDansLeGuidon #24hDansUneJournée`

{{% blockquote %}}
S'inscrire à la liste de discussion : [framalistes.org/sympa/subscribe/costrasbourg](https://framalistes.org/sympa/subscribe/costrasbourg)
{{% /blockquote %}}