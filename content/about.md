---
title: "À propos"
date: 2019-03-25T11:05:29+01:00
---

## Qui sommes-nous ?

L'idée de la démarche CoStrasbourg est née dans les têtes Tom de Communecter et Jibé (sans-bannière), mais nous sommes nombreu⋅x⋅ses à nous intérresser à la mise en réseau (vous en faites sûrement partie).

Notre ambition est que cette coopération soit portée collectivement. Dès que la dynamique sera appropriée par les organisations de Strasbourg et qu'elle s'auto-alimentera grâce à la contribution de chacun, notre objectif sera atteint ! Le but ultime étant que les alternatives écologiques et solidaires deviennent la norme.

Comptez sur nous pour vous fournir exclusivement des logiciels libres et des données ouvertes ;). Niveau gouvernance, on propose systématiquement des modes d'auto-organisation. Nous privilégions le modèle de confédération et nous refusons la création de nouveaux niveaux de hiérarchie.

(aidez-nous à quitter facebook pleaaaaaase !)
