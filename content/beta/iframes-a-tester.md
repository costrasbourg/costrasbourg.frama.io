
### Communecter
Accéder au site : [communecter](https://www.communecter.org/#agenda?startDate=1554069600&endDate=1559426400&scopeType=open&cities=54c09652f6b95c1418008110)
<iframe id="agenda-communecter-strasbourg" 
    src="https://www.communecter.org/#agenda?startDate=1554069600&endDate=1559426400&scopeType=open&cities=54c09652f6b95c1418008110"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" height="500px" width="100%"></iframe>
    
### Strasbourg Curieux
Accéder au site : [strasbourg curieux](http://strasbourg.curieux.net)
<iframe id="strasbourg-curieux"
    src="http://strasbourg.curieux.net/agenda/que_faire_aujourd_hui.php"
    sandbox="allow-scripts allow-same-origin allow-top-navigation"
    allowTransparency="true" scrolling="no" 
    frameborder="0" width="100%"  height="500px" ></iframe>
