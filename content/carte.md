---
title: "Cartographie"
date: 2019-03-25T11:05:29+01:00
---

<style>
	.main-and-footer > div {
		max-width: 95%;
	}
</style>

## Carte des initiatives locales
<a href="https://www.communecter.org/network/default/index?src=https://gist.github.com/Drakone/adcb2151744c923b69a6a8247465c458/raw/coeurometropole.json" target="_blank">Ouvrir en grand</a> / <a href="https://www.communecter.org/network/default/index?src=https://gist.github.com/Drakone/5a2dd7bf93682422987889ead87760b4/raw/costrasbourg.json" target="_blank">Voir la carte du Bas-Rhin</a>

<iframe src="https://www.communecter.org/network/default/index?src=https://gist.github.com/Drakone/adcb2151744c923b69a6a8247465c458/raw/coeurometropole.json" width="100%" height="900px" frameborder="0"></iframe>

### Contribuer
Pour ajouter ou [modifier un élément](https://doc.co.tools/books/2---utiliser-loutil/page/modifier-tag) il suffit d'avoir un compte.

{{% expandable label="Catégories" level="2" %}}
Si vous souhaitez que l'initiative se retrouve dans une des catégories, utilisez au moins un tag associé :

<ul>
    <li><b>Alimentation</b> : agriculture, alimentation, nourriture, AMAP, maraude, vegan</li>
    <li><b>Solidarité</b> : ville refuge, soliguide, action sociale, solidarité</li>
    <li><b>Citoyenneté</b> : participation citoyenne, democratie participative, citoyenneté, éducation populaire</li>
    <li><b>Stück</b> (monnaie locale) : Stück</li>
    <li><b>Zéro déchet</b> : réparation, Vrac, 100% Vrac, Ressourcerie, Occasion, Fablab, RepairCafé, Recyclage</li>
    <li><b>Accepte affiche d'évènementiel</b> : AccepteAffiche</li>
    <li><b>Panneau d'expression libre</b> (à venir) : AffichageLibre</li>
</ul>
{{% /expandable %}}