---
title: "Projets"
date: 2019-03-25T11:05:29+01:00
---

{{% warning %}}
<b>Ces projets ne sont pas la propriété des initiateurs de CoStrasbourg !</b><br><br>

Nous documentons un maximum nos pratiques pour que ce soit appropriable par n'importe qui. Si vous participez à un de ces projets votre aide est bienvenue.<br><br>

ASTUCE : En survolant les liens vous trouverez une définition synthétique du projet (si elle existe :p).
{{% /warning %}}

## Évènements
- [Mutualisons !](https://hackmd.co.tools/AwUwTCDsCGbQtADgGwDNnwCybAVngEaaoDM8I0AnGCdLgQCbCoNA# "Rencontres inter-collectifs pour faire des projets en commun") : [26 juin](https://www.facebook.com/events/385928668932125/)
- [Déjeuner inter-asso](https://hackmd.io/G3EM2Mc_R7mVIWtJyZNrDA?both "Repas entre membres de collectifs pour se rencontrer") : à vous de jouer !
- [Apéro éco-coloc](https://hackmd.co.tools/s/HJDioHVjV# "Vers un réseau de colocation") : à vous de jouer !
- [Charte d'engagement solidaire pour les municipales](https://hackmd.co.tools/s/rkRmwb9uE#d%C3%A9cloisonnement-des-assosth%C3%A9matiques)

## Partage de données
- Via [Communecter](http://communecter.org "réseau social éthique")
	- [Carte des initiatives locales](../carte/ "cartographie d'organisations et projets strasbourgeois")
	- [Lieux acceptant les affiches d'évènements](https://communecter.org/#search?text=#AccepteAffiche) + panneaux d'expressions libre (projet)
	- [Lieux zéro déchet](https://zds.fr/adressesfaq/carte-zero-dechet "lieux proposant du vrac, faisant de la réparation d'objets, acceptant les contenants réutilisables, ...")
- Via Facebook
	- [Évènements locaux par thématique](../agenda)
- Via OpenStreetMap
	- [Magasins vrac](https://cartovrac.fr/)
- [Référencement de salles de réunion](https://www.communecter.org/#@costrasbourgSalles) ([demander le lien](mailto:jibe.boh@orange.fr)) + [guide pdf](https://www.facebook.com/groups/2171571459596257/permalink/2209946379092098/)

## Mutualisation de matériel
- [Échanges de livres](https://inventaire.io/groups/lectures-climatiques-strasbourg)
- Inventaire de matériels ([demander le lien](mailto:jibe.boh@orange.fr))
- Inventaire de machines ([demander le lien](mailto:jibe.boh@orange.fr))

## Communication inter-collectifs
- [Liste de discussions mail](https://framalistes.org/sympa/info/costrasbourg)
- [Rôle interface pour les demandes des autres collectifs](https://hackmd.co.tools/KYZgbATArAjFDsBaAHAYxgQ0QFgEYDNlFcIBOUxYfMTCABhABN5cwg==#) / [Boussoles](https://www.facebook.com/groups/2171571459596257/permalink/2208220039264732/)
- [Achat d'abonnements en commun](https://www.facebook.com/groups/2171571459596257/permalink/2214106508676085/)
- [Appels à coup de mains](https://hackmd.co.tools/s/rkRmwb9uE#identifier-les-possibilit%C3%A9s-d%E2%80%99engagement)

## Collaboration et entraide
- Traduction de contenus libres et open source (idée)
- [Recrutement de bénévoles](https://www.communecter.org/#@costrasbourgRecrutement)
- [Démarchage de commerçants](https://www.communecter.org/#@costrasbourgDemarchage)
- [Collage décentralisé d'affiches](https://www.facebook.com/groups/2171571459596257/permalink/2215387065214696/)

## Guides pratiques
- Communication
	- [Ajouter un bouton "Envoyer un email" aux pages Facebook](https://hackmd.co.tools/MwNgxgjAHCBmEFoBGIAmiAsB2AhgVgRzCgFMExgAGYLPELMDDEIA/slide)
	- [Exploiter Facebook pour monter un groupe projet](https://hackmd.co.tools/IYdgpiCcBGDGCMBaaBWSAORAWdsDMikAZpCoiFgAxETwBMAJugGyRA==#)
- [Organiser un évènement éthique et populaire à Strasbourg](https://hackmd.co.tools/EwdgrGAswJwCYFoDGwCGA2Bkw2AgRpABxILgDMAZifqlZcEA?view)

## Mutualisation de services
- [Outils numériques libres](https://forum.arn-fai.net/t/degafamiser-strasbourg/210)
- Formulaires d'adhésions partagés (idée)