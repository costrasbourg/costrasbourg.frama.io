---
title: "Services"
date: 2019-03-27T12:55:55+01:00
draft: false
---

Pour aider votre collectif à se lancer dans des pratiques de mutualisation nous proposons une série d'atelier "clé en main". Bien sûr le format peut s'adapter à vos envies / besoins. Nous demandons simplement qu'au moins une personne du collectif soit présente.
Ce sont généralement des temps conviviaux ouvert à toutes et tous.

## Ouvrir sa communauté
_3h - Identification et mise en partage des ressources d'un collectif._

* Inventaire du matériel prétable (livres, outils, ...)
* Présentation et mise à jour de bases de données ouvertes ([carte des initiatives](https://news.defricheurs.fr/carte/), [échanges de livres](https://inventaire.io/groups/lectures-climatiques-strasbourg), ...)
* Présentation d'espaces d'échanges inter-asso (lieux, plateformes web, listes mail, ...)
* ...

## Documentez ses pratiques
_2h - Particulièrement utile pour les communautés fédérées souhaitant partager ces connaissances avec les autres membres de sa fédération._

* Identification de pratiques pertinentes à partager
* Co-écriture de documentation
* Contribution au guide "[organiser un évènement éthique et populaire à Strasbourg](https://hackmd.co.tools/EwdgrGAswJwCYFoDGwCGA2Bkw2AgRpABxILgDMAZifqlZcEA?view)"
* ...

## Atelier de contribution citoyenne
_De 1h à 4h - Réalisable lors d'évènements publics._

Le but est de faire participer un maximum de personnes à une base de connaissance utile à toutes et tous (généralement une cartographie).

-

En amont de l'évènement :

* définir un thème (zéro déchet, culture, transport, alimentation...)
* trouver un lieu cohérent avec le thème
* communiquer

Le jour-j prévoir :

* quelques ordinateurs (selon nombres de personnes attendues)
* vidéoprojecteur (si possible)
* feutres et post-it
